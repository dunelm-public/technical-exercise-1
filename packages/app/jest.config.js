export default {
  rootDir: process.cwd(),
  testRegex: '(/__tests__/.*|(\\.|/)(test|spec|api))\\.jsx?$',
  testEnvironment: 'jsdom',
}
