import React from 'react'
import { createRoot } from 'react-dom/client'
import App from './App'

const renderApplication = (Component) => {
  const root = createRoot(document.getElementById('content'))
  root.render(<Component />)
}

if (process.env.NODE_ENV === 'development' && module.hot) {
  module.hot.accept('./App.jsx', () =>
    renderApplication(require('./App').default),
  )
}

renderApplication(App)
